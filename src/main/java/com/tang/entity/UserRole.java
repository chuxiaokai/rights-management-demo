package com.tang.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (UserRole)实体类
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@Data
@Accessors(chain = true)
@TableName("user_role")
@ApiModel(value="user_role对象", description="")
public class UserRole implements Serializable {
    private static final long serialVersionUID = -35811128117586430L;
   

    /**
    * 主键
    */    
    @ApiModelProperty(value = "主键")
    @TableField("id")
    private Integer id;

    /**
    * 用户id
    */    
    @ApiModelProperty(value = "用户id")
    @TableField("uid")
    private Integer uid;

    /**
    * 角色id
    */    
    @ApiModelProperty(value = "角色id")
    @TableField("rid")
    private Integer rid;

        
        
        

}
package com.tang.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (MenuRole)实体类
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@Data
@Accessors(chain = true)
@TableName("menu_role")
@ApiModel(value="menu_role对象", description="")
public class MenuRole implements Serializable {
    private static final long serialVersionUID = 452843022151728321L;
   

    /**
    * 主键
    */    
    @ApiModelProperty(value = "主键")
    @TableField("id")
    private Integer id;

    /**
    * 菜单ID
    */    
    @ApiModelProperty(value = "菜单ID")
    @TableField("menu_id")
    private Integer menuId;

    /**
    * 角色ID
    */    
    @ApiModelProperty(value = "角色ID")
    @TableField("role_id")
    private Integer roleId;

        
        
        

}
package com.tang.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * (Users)实体类
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@Data
@Accessors(chain = true)
@TableName("users")
@ApiModel(value="users对象", description="")
public class Users  implements Serializable, UserDetails {
    private static final long serialVersionUID = -32077562888173233L;
   

    /**
    * 主键
    */    
    @ApiModelProperty(value = "主键")
    @TableField("id")
    private Integer id;

    /**
    * 登录名
    */    
    @ApiModelProperty(value = "登录名")
    @TableField("username")
    private String username;

    /**
    * 密码
    */    
    @ApiModelProperty(value = "密码")
    @TableField("password")
    private String password;

    /**
    * 昵称
    */    
    @ApiModelProperty(value = "昵称")
    @TableField("name")
    private String name;


    /**
     * 角色
     */
    @ApiModelProperty(value = "角色集合")
    private List<Role> roles;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream()
                .map(roles -> new SimpleGrantedAuthority(roles.getCharacterName())).collect(Collectors.toList());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", roles=" + roles +
                '}';
    }
}
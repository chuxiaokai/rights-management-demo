package com.tang.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Menu)实体类
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@Data
@Accessors(chain = true)
@TableName("menu")
@ApiModel(value="menu对象", description="")
public class Menu implements Serializable {
    private static final long serialVersionUID = 715807364750423823L;
   

    /**
    * 主键
    */    
    @ApiModelProperty(value = "主键")
    @TableField("id")
    private Integer id;

    /**
    * 路径
    */    
    @ApiModelProperty(value = "路径")
    @TableField("path")
    private String path;

    /**
    * 父ID
    */    
    @ApiModelProperty(value = "父ID")
    @TableField("parent_id")
    private Integer parentId;

    /**
    * 菜单名称
    */    
    @ApiModelProperty(value = "菜单名称")
    @TableField("name")
    private String name;

        
        
        
        

}
package com.tang.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Role)实体类
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@Data
@Accessors(chain = true)
@TableName("role")
@ApiModel(value="role对象", description="")
public class Role implements Serializable {
    private static final long serialVersionUID = 722875581091143831L;
   

    /**
    * 主键
    */    
    @ApiModelProperty(value = "主键")
    @TableField("id")
    private Integer id;

    /**
    * 角色名称ROLE_开头
    */    
    @ApiModelProperty(value = "角色名称ROLE_开头")
    @TableField("character_name")
    private String characterName;

    /**
    * 角色实际名称
    */    
    @ApiModelProperty(value = "角色实际名称")
    @TableField("name")
    private String name;

        
        
        

}
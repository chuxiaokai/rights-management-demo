package com.tang.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tang.vo.ReturnBean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 权限不足
 * @author tang
 * @date 2021/4/6 12:40
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {


    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        //内容格式为json
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        ReturnBean returnBean = ReturnBean.error("权限不足");
        returnBean.setCode(401L);
        //获得输出流
        PrintWriter writer = response.getWriter();
        writer.write(new ObjectMapper().writeValueAsString(returnBean));
        writer.flush();
        writer.close();
    }
}

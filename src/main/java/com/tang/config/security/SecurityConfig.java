package com.tang.config.security;

import com.tang.dao.UsersDao;
import com.tang.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.nio.file.attribute.UserPrincipalNotFoundException;

/**
 * @author tang
 * @date 2021/4/5 20:23
 */
@Configurable
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UsersDao usersDao;

    @Autowired
    private RestAccessDeniedHandler restAccessDeniedHandler;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;



    /**
     * 放行资源
     * 不会走过滤器链
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/login",
                "/logout",
                "/hello",
                "/doc.html",
                "/js/**",
                "/css/**",
                "/favicon.ico",
                "/index.html",
                "/webjars/**",
                "/code",
                "/swagger-resources/**",
                "/v2/api-docs/**",
                "/code");
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
       auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //关闭csrf和不使用session
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //权限认证,任何请求都需要被认证，并且设置动态权限
        http.authorizeRequests().anyRequest().authenticated();

        //不使用缓存
        http.headers().cacheControl();

        //token过滤器,作为用户名密码验证过滤器
        http.addFilterBefore(jwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

        //未登录或者token失效和权限不足处理程序
        http.exceptionHandling()
                //权限不足
                .accessDeniedHandler(restAccessDeniedHandler)
                //未登录或者token过期
                .authenticationEntryPoint(restAuthenticationEntryPoint);
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService(){
        return  username -> {
            System.out.println("进入重写了userDetailsService");
            Users users = usersDao.fullQuery(username);
            if (users == null){
                throw new UsernameNotFoundException("用户名不存在");
            }
            System.out.println("获取到登录用户信息：" + users);
            return users;
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Bean
    public JwtTokenFilter jwtTokenFilter(){
        return new JwtTokenFilter();
    }
}

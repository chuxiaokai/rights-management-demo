package com.tang.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author tang
 * @date 2021/4/6 13:51
 */
@ApiModel(value = "登录实体类",description = "")
@Data
public class Login {

    @ApiModelProperty(value = "账号",readOnly = true)
    private String username;

    @ApiModelProperty(value = "密码",readOnly = true)
    private String password;

    @ApiModelProperty(value = "验证码",readOnly = true)
    private String code;

}

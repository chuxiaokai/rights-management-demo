package com.tang.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 公共返回类
 * @author tang
 * @date 2021/4/6 12:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(value = "公共返回类",description = "")
public class ReturnBean {

    @ApiModelProperty(value = "状态码")
    private Long code;

    @ApiModelProperty(value = "信息")
    private String info;

    @ApiModelProperty(value = "数据")
    private Object date;

    /**
     * 成功
     * @param info
     * @param date
     * @return
     */
    public static  ReturnBean success(String info,Object date){
        return new ReturnBean(200L,info,date);
    }

    /**
     * 成功
     * @param info
     * @return
     */
    public static  ReturnBean success(String info){
        return new ReturnBean(200L,info,null);
    }

    /**
     * 失败
     * @param info
     * @param date
     * @return
     */
    public static  ReturnBean error(String info,Object date){
        return new ReturnBean(500L,info,date);
    }

    /**
     * 失败
     * @param info
     * @return
     */
    public static  ReturnBean error(String info){
        return new ReturnBean(500L,info,null);
    }

}

package com.tang.controller;

import com.tang.entity.Users;
import com.tang.service.UsersService;
import com.tang.vo.Login;
import com.tang.vo.ReturnBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * (Users)表控制层
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@RestController
@Api(value = "用户控制层")
public class UsersController {
    /**
     * 服务对象
     */
    @Resource
    private UsersService usersService;


    @ApiOperation(value = "登录方法，成功返回token")
    @PostMapping("/login")
    public ReturnBean login(@RequestBody Login login, HttpServletRequest request){
        System.out.println("进入控制层登录方法");
        ReturnBean returnBean = usersService.login(login, request);
        System.out.println("获取到的上下文信息"+SecurityContextHolder.getContext().getAuthentication());
        System.out.println("离开控制层登录方法");
        return returnBean;
    }

    @ApiOperation(value = "Security从上下文中获取用户信息")
    @PostMapping("/getUserInfo")
     public Users  getUserInfo(){
        Users user =  (Users)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        user.setPassword(null);
        return user;
    }

}
package com.tang.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tang
 * @date 2021/4/7 15:46
 */
@RestController
@Api(value = "测试控制类")
public class HelloController {


    /**
     * 前端返回的token格式:
     *  Bearer  eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTc3ODMwNTQsInN1YiI6ImFkbWluIiwiY3JlYXRlZCI6MTYxNzc4MjY5NDE4N30.WFNNp3I2o-QFdCSaRZAf0m7Q6uODwBfouvSGcsVahIs
     * @return
     */
    @PreAuthorize("hasRole('ROLE_admin')")
    @PostMapping("/hello01")
    @ApiOperation(value = "测试01")
    public String test01(){
        return "ok01";
    }

    @PreAuthorize("hasRole('ROLE_user')")
    @PostMapping("/hello02")
    @ApiOperation(value = "测试02")
    public String test02(){
        return "ok02";
    }

}

package com.tang.exception;

import com.tang.vo.ReturnBean;
import io.jsonwebtoken.ClaimJwtException;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 * @author tang
 * @date 2021/4/7 13:43
 */
@RestControllerAdvice
public class GlobalException {

    @ExceptionHandler(value = AccountExpiredException.class)
    public ReturnBean expiredJwtException(){
        System.out.println("121212121212");
         ReturnBean returnBean = ReturnBean.error("登录已过期,请重新登录");
        returnBean.setCode(417L);
        return returnBean;
    }
}

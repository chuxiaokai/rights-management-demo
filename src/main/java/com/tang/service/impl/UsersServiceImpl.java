package com.tang.service.impl;

import com.tang.config.security.JwtTokenUtil;
import com.tang.dao.UsersDao;
import com.tang.entity.Users;
import com.tang.service.UsersService;
import com.tang.vo.Login;
import com.tang.vo.ReturnBean;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Users)表服务实现类
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
@Service("usersService")
public class UsersServiceImpl implements UsersService {
    @Resource
    private UsersDao usersDao;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;


    @Autowired
    private PasswordEncoder passwordEncoder;


    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Override
    public ReturnBean login(Login login, HttpServletRequest request) {
        if (login == null || login.getUsername() == null || login.getPassword() == null){
            return ReturnBean.error("用户名密码为空");
        }

        String code = (String) request.getServletContext().getAttribute("code");
        if (code== null || !code.equalsIgnoreCase(login.getCode())){
            return ReturnBean.error("验证码错误");
        }


        UserDetails user = null;
        try {
            user = userDetailsService.loadUserByUsername(login.getUsername());
        } catch (UsernameNotFoundException e) {
            return ReturnBean.error(e.getMessage());
        }
        if (! passwordEncoder.matches(login.getPassword(), user.getPassword())){
            return ReturnBean.error("密码错误");
        }

        //更新security登录用户信息
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new
                UsernamePasswordAuthenticationToken(user,"null",user.getAuthorities());
        //security上下文
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);


        //生成token
        String token = jwtTokenUtil.generateToken(user);
        Map<String, Object> map = new HashMap<>(2);
        map.put("token",token);
        map.put("tokenHeader",tokenHeader);
        System.out.println(jwtTokenUtil.refresh(token));
        return ReturnBean.success("登录成功", map);
    }
}
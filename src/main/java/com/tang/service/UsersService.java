package com.tang.service;

import com.tang.entity.Users;
import com.tang.vo.Login;
import com.tang.vo.ReturnBean;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * (Users)表服务接口
 *
 * @author makejava
 * @since 2021-04-05 19:52:51
 */
public interface UsersService {



    /**
     * 登录
     * @param login
     * @return
     */
    ReturnBean login(Login login, HttpServletRequest request);
}
/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : contact

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 06/04/2021 11:03:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '路径',
  `parent_id` int(11) NOT NULL COMMENT '父ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '/sys/system/**', 0, '系统管理');
INSERT INTO `menu` VALUES (2, '/order/**', 0, '订单系统');
INSERT INTO `menu` VALUES (3, '/sys/system/user', 1, '用户管理');
INSERT INTO `menu` VALUES (4, '/sys/system/personal', 1, '个人中心');
INSERT INTO `menu` VALUES (5, '/order/review', 2, '订单审核');
INSERT INTO `menu` VALUES (6, '/order/placeAnOrder', 2, '下单');

SET FOREIGN_KEY_CHECKS = 1;

package com.tang;

import com.tang.dao.UsersDao;
import com.tang.entity.Users;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private UsersDao usersDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    void contextLoads() {
        Users users = usersDao.fullQuery("admin");
        System.out.println(users);
       //List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) users.getAuthorities();
        //authorities.forEach(System.out::println);
    }

    @Test
    void name() {
        System.out.println(passwordEncoder.encode("123"));
    }
}

# 权限管理demo

#### 介绍
基于Spring Security和JWT的前后端分离权限demo。
后端使用Spring Boot,swagger2接口文档,结合mybatis，mysql，redis（主要是菜单缓存）,druid数据库连接池

#### 软件架构

前后端分离项目，后端代码

#### 安装教程

资源目录下有sql文件

#### 使用说明

剥离了业务，做一个纯粹的权限系统

一共5张表：用户表,用户角色表，角色表，角色菜单表，菜单表

逻辑：
    登录（username,password,code） --  生成token--每次访问的请求头需要携带token -- 后端验证token -- 是否刷新token -- 访问资源

    
    

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
